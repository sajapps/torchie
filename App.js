/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Button } from 'react-native-material-ui';
import { BottomNavigation } from 'react-native-material-ui';
import Torch from 'react-native-torch';
import {
  ScrollView,
  Slider,
  StyleSheet,
  Text,
  View
} from 'react-native';

type Props = {};
export default class App extends Component<Props> {
  torchTimeout = null
  torchState = null;
  state = {
    strobeInterval: 1000,
    mode: null,
  }

  turnOffTorch() {
    Torch.switchState(false);
    clearTimeout(this.torchTimeout);
  }

  turnOnTorch() {
    Torch.switchState(true);
  }

  turnOnStrobe() {
    const _turnOnStrobe = () => {
      clearTimeout(this.torchTimeout);
      this.torchTimeout = setTimeout(() => {
        console.info('called');
        Torch.switchState(!this.torchState);
        this.torchState = !this.torchState;
        _turnOnStrobe();
      }, this.state.strobeInterval);
    };
    _turnOnStrobe();
  }

  render() {
    switch (this.state.mode) {
      case 'on':
        this.turnOnTorch();
        break;
      case 'off':
        this.turnOffTorch();
        break;
      case 'strobe':
        this.turnOnStrobe();
        break;
    }
    return (
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="interactive"
          contentContainerStyle={{
            flex: 1,
            alignContent: 'flex-end',
            justifyContent: 'center'
          }}
        >
          <View>
            <Button text="Turn on" onPress={() => this.setState({mode: 'on'})} />
            <Button text="Turn off" onPress={() => this.setState({mode: 'off'})} />
            <Button text="Strobe" onPress={() => this.setState({mode: 'strobe'})} />
            <Slider
              value={this.state.strobeInterval}
              maximumValue={2000}
              minimumValue={1}
              onValueChange={(value) => this.setState({strobeInterval: value})}
            />
            <Text>
              Strobe Interval: {this.state.strobeInterval}
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
